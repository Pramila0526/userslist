
import React from 'react';
import '../Css/User.css'
import TextField from '@material-ui/core/TextField';

 interface Employees{
     name : String
 }

 interface EmpListProps{
    initialTasks : Employees[]
 }

 interface EmpListStates{
     emps: Employees[]
 }

 class EmpsList extends React.Component<
 EmpListProps,
 EmpListStates 
 >{
     constructor(props : EmpListProps){
         super(props);
         this.state = {
          emps: props.initialTasks
         };

         //Binding onAddNewTasksClick() in Constructor
         //This way we make sure that the instance object is bound to this(onAddNewTasksClick) method
         this.onAddNewEmployeesClick = this.onAddNewEmployeesClick.bind(this)
     }
     
     onAddNewEmployeesClick() {
        this.setState({
            emps: [
                ...this.state.emps,
                {name : 'Rajendra Sharma' }  
            ]
        });
     }
     

    render() {
        const { emps } = this.state;
        return (
            <div className="wholeBackGround"> 
            <div className="userList">
            <ul> 
                {emps.map((emp,i) => {
                    return <li key={i}>{emp.name}</li>;
                })}

                <br>
                </br>

                <TextField  
                                        size="small"
                                        name="name"
                                        id="outlined"
                                        label="Name"
                                        variant="outlined"
                                        style={{ width: "20%" }}
                                       >Add New Employees</TextField>
                                       <br></br>
                <button onClick={this.onAddNewEmployeesClick}>Add New Employees</button>
                
            </ul>
            </div>
            </div>
         );
    }
 
 }
 const emps = [
     {name :'Khushi Singh' },
     {name :'Meena Gupta' },
     {name :'Devendra Sharma' },
     {name :'Prajakta Patil' },
     {name :'Gayatri Soni' },
     {name :'Meena Kumar' },
     {name :'Pratik Shah' },
    
 ];

export default () => (
    <div>
        <EmpsList initialTasks={emps}/>
            </div>
)